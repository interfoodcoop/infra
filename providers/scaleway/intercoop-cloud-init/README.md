This environment `intercoop-cloud-init` was used only once to create the Object Storage space for future terraform runs.

These files are here only for documentation purposes and/or for future initialisation if someone wants to start an infra from scratch.
