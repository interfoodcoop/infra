locals {
  region = "fr-par"
}

# Initial bucket definition (created once at the very first terraform run)
resource "scaleway_object_bucket" "terraform-state" {
  name   = "intercoop-terraform"
  acl    = "private"
  region = local.region
}
