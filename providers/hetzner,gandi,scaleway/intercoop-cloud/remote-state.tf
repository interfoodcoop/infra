terraform {
  backend "s3" {
    bucket                      = "intercoop-terraform"
    key                         = "hetzner,gandi,scaleway/intercoop-cloud/terraform.tfstate"
    region                      = "fr-par"
    endpoint                    = "https://s3.fr-par.scw.cloud"
    skip_credentials_validation = true
    skip_region_validation      = true
  }
}
