terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = ">= 1.21.0"
    }
    gandi = {
      version = ">= 2.0.0"
      source  = "github.com/go-gandi/gandi"
    }
  }
  required_version = ">= 0.13"
}
