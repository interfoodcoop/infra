############################################################
# List of public SSH keys which have access to the servers #
############################################################

resource "hcloud_ssh_key" "paul_polonium" {
  name       = "paul_polonium"
  public_key = file("../../../public_keys/@paulrbr:matrix.org.pub")
}

resource "hcloud_ssh_key" "jacques_elitebook" {
  name       = "jacques_elitebook"
  public_key = file("../../../public_keys/@jacques_coop14:interfoodcoop.net.pub")
}

resource "hcloud_ssh_key" "martin_macbook" {
  name       = "martin_macbook"
  public_key = file("../../../public_keys/@martinszinte:martinszinte.net.pub")
}
