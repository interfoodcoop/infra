# Authentication is done via either gandi classical env variables (GANDI_SHARING_ID & GANDI_KEY)
# or via pass password manager:
# - Store your gandi sharing id in 'pass terraform/gandi/intercoop-cloud/access_key' path
# - Store your gandi api key in 'pass terraform/gandi/intercoop-cloud/secret' path
provider "gandi" {
}

# Temporary domain for testing
locals {
  domain = "interfoodcoop.net"
  alt_domains = [
    "interfoodcoop.net",
  ]
  alt_www_domains = [
  ]
}

##############################################
# Generic interfoodcoop.net settings #
##############################################

resource "gandi_livedns_record" "mailjet-interfoodcoop-net-TXT" {
  zone   = local.domain
  name   = "mailjet._733bc383"
  type   = "TXT"
  ttl    = 3600
  values = ["733bc383e511ff7612bdce3aee5fb269"]
}

resource "gandi_livedns_record" "mailjet-SPF-interfoodcoop-net-TXT" {
  zone   = local.domain
  name   = "@"
  type   = "TXT"
  ttl    = 3600
  values = ["v=spf1 include:spf.mailjet.com include:_mailcust.gandi.net ?all"]
}

resource "gandi_livedns_record" "mailjet-DKIM-interfoodcoop-net-TXT" {
  zone   = local.domain
  name   = "mailjet._domainkey"
  type   = "TXT"
  ttl    = 3600
  values = ["k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCuKSKgVD29d1IGE+b5hzwLnFVZeJXHZHSZ64d4cFv3lZeL+TdPj2vChKjgtYf4wuMgFVZOCinDb7/h5qVVZebAZLnO2hhM30UyY8AvY84btjfycWiAYRZLt7rjJUEzj70//7d++V/Ajd4yZRhfieGDijI3VONq+Xv/2AKrXT0jIQIDAQAB"]
}

# Used for GandiMail (bonjour@interfoodcoop.net or contact*@interfoodcoop.net)
resource "gandi_livedns_record" "interfoodcoop-net-MX" {
  zone   = local.domain
  name   = "@"
  type   = "MX"
  ttl    = 3600
  values = ["10 spool.mail.gandi.net.", "50 fb.mail.gandi.net."]
}

####################
# Nextcloud domain #
####################

resource "gandi_livedns_record" "nuage-interfoodcoop-net-A" {
  count  = local.nextcloud_server_count > 0 ? 1 : 0
  zone   = local.domain
  name   = "nuage"
  type   = "A"
  ttl    = 3600
  values = hcloud_server.nextcloud[*].ipv4_address
}
resource "gandi_livedns_record" "nuage-interfoodcoop-net-AAAA" {
  count  = local.nextcloud_server_count > 0 ? 1 : 0
  zone   = local.domain
  name   = "nuage"
  type   = "AAAA"
  ttl    = 3600
  values = hcloud_server.nextcloud[*].ipv6_address
}

resource "gandi_livedns_record" "nextcloud-priv-interfoodcoop-net-A" {
  count  = local.nextcloud_server_count
  zone   = local.domain
  name   = "nextcloud${count.index + 1}.priv"
  type   = "A"
  ttl    = 600
  values = [element(hcloud_server_network.nextcloud-private-ip.*.ip, count.index)]
}

# Mailjet configuration
resource "gandi_livedns_record" "mailjet-nuage-interfoodcoop-net-TXT" {
  zone   = local.domain
  name   = "mailjet._f78255a4.nuage"
  type   = "TXT"
  ttl    = 3600
  values = ["f78255a4aaf6e516d45e320dc9f1df36"]
}

resource "gandi_livedns_record" "mailjet-SPF-nuage-interfoodcoop-net-TXT" {
  zone   = local.domain
  name   = "nuage"
  type   = "TXT"
  ttl    = 3600
  values = ["v=spf1 include:spf.mailjet.com ?all"]
}

resource "gandi_livedns_record" "mailjet-DKIM-nuage-interfoodcoop-net-TXT" {
  zone   = local.domain
  name   = "mailjet._domainkey.nuage"
  type   = "TXT"
  ttl    = 3600
  values = ["k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC4fxrfO85mCTs3LZsCx8N9Dw5cl6lSOyWVQjA4iFyHHsbybh0PkdElJ9cXajccRAQYxhTWLL2UuzOABhlcbypqqU2WDWW7k3jXQc2ShEABhrHNGcjPK5nayOb4b64aNTvnk7BSixJ8FuvyCXDKG/lIeroQ//DhgoY4zgy5cep1QQIDAQAB"]
}

#################
# Guides domain #
#################

# Legacy domain redirect wiki. → guides.
resource "gandi_livedns_record" "wiki-interfoodcoop-net-A" {
  count  = local.nextcloud_server_count > 0 ? 1 : 0
  zone   = local.domain
  name   = "wiki"
  type   = "A"
  ttl    = 3600
  values = hcloud_server.nextcloud[*].ipv4_address
}
# Legacy domain redirect wiki. → guides.
resource "gandi_livedns_record" "wiki-interfoodcoop-net-AAAA" {
  count  = local.nextcloud_server_count
  zone   = local.domain
  name   = "wiki"
  type   = "AAAA"
  ttl    = 3600
  values = hcloud_server.nextcloud[*].ipv6_address
}

resource "gandi_livedns_record" "guides-interfoodcoop-net-A" {
  count  = local.nextcloud_server_count > 0 ? 1 : 0
  zone   = local.domain
  name   = "guides"
  type   = "A"
  ttl    = 3600
  values = hcloud_server.nextcloud[*].ipv4_address
}
resource "gandi_livedns_record" "guides-interfoodcoop-net-AAAA" {
  count  = local.nextcloud_server_count
  zone   = local.domain
  name   = "guides"
  type   = "AAAA"
  ttl    = 3600
  values = hcloud_server.nextcloud[*].ipv6_address
}

resource "gandi_livedns_record" "mailjet-guides-interfoodcoop-net-TXT" {
  zone   = local.domain
  name   = "mailjet._ac540cdb.guides"
  type   = "TXT"
  ttl    = 3600
  values = ["ac540cdbf636608f82f97bc3de687c4c"]
}

resource "gandi_livedns_record" "mailjet-SPF-guides-interfoodcoop-net-TXT" {
  zone   = local.domain
  name   = "guides"
  type   = "TXT"
  ttl    = 3600
  values = ["v=spf1 include:spf.mailjet.com ?all"]
}

resource "gandi_livedns_record" "mailjet-DKIM-guides-interfoodcoop-net-TXT" {
  zone   = local.domain
  name   = "mailjet._domainkey.guides"
  type   = "TXT"
  ttl    = 3600
  values = ["k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDmIi40PqPT3o4BGK1h4aNSwc7FsG3U8+RjSl7BWqXuRKWLV696GUgyTceI6GBjZ9BH8qm1BcsEusg1CTT0MOetV2KmdUHqTU3H6WSoIixTX3ZorKwhZ8BIDsgWrds79gM+KHfoBi8PX9Kh0hIl0neE3/+0edVL+dNWt+eNptD6aQIDAQAB"]
}

########################
# SimpleSAMLphp domain #
########################

resource "gandi_livedns_record" "auth-interfoodcoop-net-A" {
  count  = local.nextcloud_server_count > 0 ? 1 : 0
  zone   = local.domain
  name   = "auth"
  type   = "A"
  ttl    = 3600
  values = hcloud_server.nextcloud[*].ipv4_address
}
resource "gandi_livedns_record" "auth-interfoodcoop-net-AAAA" {
  count  = local.nextcloud_server_count > 0 ? 1 : 0
  zone   = local.domain
  name   = "auth"
  type   = "AAAA"
  ttl    = 3600
  values = hcloud_server.nextcloud[*].ipv6_address
}

######################################################################
# Forum domain                                                       #
#                                                                    #
# We should use the existing forum forum.supermarches-cooperatifs.fr #
######################################################################

resource "gandi_livedns_record" "forum-interfoodcoop-net-A" {
  count  = local.discourse_server_count > 0 ? 1 : 0
  zone   = local.domain
  name   = "forum"
  type   = "A"
  ttl    = 3600
  values = ["54.36.181.7"]
}

# Mailjet configuration
# resource "gandi_livedns_record" "mailjet-forum-interfoodcoop-net-TXT" {
#   count  = local.discourse_server_count > 0 ? 1 : 0
#   zone   = local.domain
#   name   = "mailjet._295584a3.forum"
#   type   = "TXT"
#   ttl    = 3600
#   values = ["295584a35e481b5fb374c877936bb526"]
# }

# resource "gandi_livedns_record" "mailjet-SPF-forum-interfoodcoop-net-TXT" {
#   count  = local.discourse_server_count > 0 ? 1 : 0
#   zone   = local.domain
#   name   = "forum"
#   type   = "TXT"
#   ttl    = 3600
#   values = ["v=spf1 include:spf.mailjet.com ?all"]
# }

# resource "gandi_livedns_record" "mailjet-DKIM-forum-interfoodcoop-net-TXT" {
#   count  = local.discourse_server_count > 0 ? 1 : 0
#   zone   = local.domain
#   name   = "mailjet._domainkey.forum"
#   type   = "TXT"
#   ttl    = 3600
#   values = ["k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDJlhvOTWzf/PPkYzBMv1l5axVzizMxeyyA5IRo1e0LpqwhUYciDfxeHoK527uesSBq35jSj78RIfs5ZNOpfBfKOObyu0UoG3JMNKrh3zcu1QNZEBTW1cghDaC9zky2V/4bIY2AIGq4zTUozyRR0qADECqDQfo4WOC8RDMfoGpjrwIDAQAB"]
# }

##############
# WWW domain #
##############

resource "gandi_livedns_record" "gitlab-pages-verifications-interfoodcoop-net-TXT" {
  zone   = local.domain
  name   = "_gitlab-pages-verification-code.www"
  type   = "TXT"
  ttl    = 3600
  values = ["gitlab-pages-verification-code=fe7bed598596ede5409d395d76b23602"]
}

resource "gandi_livedns_record" "interfoodcoop-net-CNAME" {
  zone   = local.domain
  name   = "www"
  type   = "CNAME"
  ttl    = 3600
  values = ["interfoodcoop.gitlab.io."]
}

# Redirects

resource "gandi_livedns_record" "alt-interfoodcoop-A" {
  for_each = toset(local.alt_domains)
  zone     = each.key
  name     = "@"
  type     = "A"
  ttl      = 3600
  values   = hcloud_server.nextcloud[*].ipv4_address
}

resource "gandi_livedns_record" "alt-interfoodcoop-AAAA" {
  for_each = toset(local.alt_domains)
  zone     = each.key
  name     = "@"
  type     = "AAAA"
  ttl      = 3600
  values   = hcloud_server.nextcloud[*].ipv6_address
}

resource "gandi_livedns_record" "alt-www-interfoodcoop-A" {
  for_each = toset(local.alt_www_domains)
  zone     = each.key
  name     = "www"
  type     = "A"
  ttl      = 3600
  values   = hcloud_server.nextcloud[*].ipv4_address
}

resource "gandi_livedns_record" "alt-www-interfoodcoop-AAAA" {
  for_each = toset(local.alt_www_domains)
  zone     = each.key
  name     = "www"
  type     = "AAAA"
  ttl      = 3600
  values   = hcloud_server.nextcloud[*].ipv6_address
}

######################
# Rocket.Chat domain #
######################

resource "gandi_livedns_record" "rocket-interfoodcoop-net-A" {
  count  = local.rocket_server_count > 0 ? 1 : 0
  zone   = local.domain
  name   = "rocket"
  type   = "A"
  ttl    = 3600
  values = hcloud_server.rocket[*].ipv4_address
}

resource "gandi_livedns_record" "rocket-interfoodcoop-net-AAAA" {
  count  = local.rocket_server_count > 0 ? 1 : 0
  zone   = local.domain
  name   = "rocket"
  type   = "AAAA"
  ttl    = 3600
  values = hcloud_server.rocket[*].ipv6_address
}

resource "gandi_livedns_record" "rocket-priv-interfoodcoop-net-A" {
  count  = local.rocket_server_count
  zone   = local.domain
  name   = "rocket${count.index + 1}.priv"
  type   = "A"
  ttl    = 600
  values = [element(hcloud_server_network.rocket-private-ip.*.ip, count.index)]
}

####################
# Collabora domain #
####################

resource "gandi_livedns_record" "collabora-interfoodcoop-net-A" {
  count  = local.collabora_server_count > 0 ? 1 : 0
  zone   = local.domain
  name   = "collabora"
  type   = "A"
  ttl    = 3600
  values = hcloud_server.collabora[*].ipv4_address
}

resource "gandi_livedns_record" "collabora-interfoodcoop-net-AAAA" {
  count  = local.collabora_server_count > 0 ? 1 : 0
  zone   = local.domain
  name   = "collabora"
  type   = "AAAA"
  ttl    = 3600
  values = hcloud_server.collabora[*].ipv6_address
}

resource "gandi_livedns_record" "collabora-priv-interfoodcoop-net-A" {
  count  = local.collabora_server_count
  zone   = local.domain
  name   = "collabora${count.index + 1}.priv"
  type   = "A"
  ttl    = 600
  values = [element(hcloud_server_network.collabora-private-ip.*.ip, count.index)]
}

#################
# Matrix domain #
#################

resource "gandi_livedns_record" "matrix-interfoodcoop-net-A" {
  count  = local.matrix_server_count > 0 ? 1 : 0
  zone   = local.domain
  name   = "matrix"
  type   = "A"
  ttl    = 3600
  values = hcloud_server.matrix[*].ipv4_address
}

# Not sure why but Docker doesn't listen on ipv6 interface
# Thus this DNS entry can create “connection refused” errors for some clients
# Trying to connect to Matrix via ipv6...
#
# resource "gandi_livedns_record" "matrix-interfoodcoop-net-AAAA" {
#   count  = local.matrix_server_count > 0 ? 1 : 0
#   zone   = local.domain
#   name   = "matrix"
#   type   = "AAAA"
#   ttl    = 3600
#   values = hcloud_server.matrix[*].ipv6_address
# }

resource "gandi_livedns_record" "matrix-priv-interfoodcoop-net-A" {
  count  = local.matrix_server_count
  zone   = local.domain
  name   = "matrix${count.index + 1}.priv"
  type   = "A"
  ttl    = 600
  values = [element(hcloud_server_network.matrix-private-ip.*.ip, count.index)]
}

resource "gandi_livedns_record" "element-interfoodcoop-net-CNAME" {
  count  = local.matrix_server_count > 0 ? 1 : 0
  zone   = local.domain
  name   = "element"
  type   = "CNAME"
  ttl    = 3600
  values = ["matrix"]
}

resource "gandi_livedns_record" "matrix-identity-tcp-interfoodcoop-net-SRV" {
  count  = local.matrix_server_count > 0 ? 1 : 0
  zone   = local.domain
  name   = "_matrix-identity._tcp"
  type   = "SRV"
  ttl    = 3600
  values = ["10       0     443 matrix.interfoodcoop.net."]
}

#################
# Bbb domain #
#################

resource "gandi_livedns_record" "bbb-interfoodcoop-net-A" {
  count  = local.bbb_server_count > 0 ? 1 : 0
  zone   = local.domain
  name   = "bbb"
  type   = "A"
  ttl    = 600 # 10 minutes only for testing
  values = hcloud_floating_ip.bbb_public_ip.*.ip_address
}

resource "gandi_livedns_record" "bbb-interfoodcoop-net-AAAA" {
  count  = local.bbb_server_count > 0 ? 1 : 0
  zone   = local.domain
  name   = "bbb"
  type   = "AAAA"
  ttl    = 600 # 10 minutes only for testing
  values = hcloud_server.bbb[*].ipv6_address
}

resource "gandi_livedns_record" "bbb-priv-interfoodcoop-net-A" {
  count  = local.bbb_server_count
  zone   = local.domain
  name   = "bbb${count.index + 1}.priv"
  type   = "A"
  ttl    = 600 # 10 minutes only for testing
  values = [element(hcloud_server_network.bbb-private-ip.*.ip, count.index)]
}
