---
# The bare domain name which represents your Matrix identity.
# Matrix user ids for your server will be of the form (`@user:<matrix-domain>`).
#
# Note: this playbook does not touch the server referenced here.
# Installation happens on another server ("matrix.<matrix-domain>").
#
# If you've deployed using the wrong domain, you'll have to run the Uninstalling step,
# because you can't change the Domain after deployment.
#
# Example value: example.com
matrix_domain: interfoodcoop.net

# Reverse proxy (and SSL certificates) handled by Traefik
matrix_playbook_reverse_proxy_type: playbook-managed-traefik
# This is something which is provided to Let's Encrypt when retrieving SSL certificates for domains.
#
# In case SSL renewal fails at some point, you'll also get an email notification there.
#
# If you decide to use another method for managing SSL certifites (different than the default Let's Encrypt),
# you won't be required to define this variable (see `docs/configuring-playbook-ssl-certificates.md`).
#
# Example value: someone@example.com
devture_traefik_config_certificatesResolvers_acme_email: "{{ lets_encrypt_support_email }}"

# A shared secret (between Coturn and Synapse) used for authentication.
# You can put any string here, but generating a strong one is preferred (e.g. `pwgen -s 64 1`).
matrix_coturn_turn_static_auth_secret: "{{ vault_matrix_coturn_turn_static_auth_secret }}"

# A secret used to protect access keys issued by the server.
# You can put any string here, but generating a strong one is preferred (e.g. `pwgen -s 64 1`).
matrix_synapse_macaroon_secret_key: "{{ vault_matrix_synapse_macaroon_secret_key }}"

# All secrets keys are now derived from `matrix_homeserver_generic_secret_key`, not from `matrix_synapse_macaroon_secret_key`.
# To keep them all the same, define `matrix_homeserver_generic_secret_key` in terms of `matrix_synapse_macaroon_secret_key`.
# Using a new secret value for this configuration key is also possible and should not cause any problems.
#
# Fun fact: new installations (based on the new `examples/vars.yml` file) do this in reverse.
# That is, the Synapse macaroon secret is derived from `matrix_homeserver_generic_secret_key`.
matrix_homeserver_generic_secret_key: "{{ vault_matrix_synapse_macaroon_secret_key }}"

# A secret used to protect access to the postgresql database.
# You can put any string here, but generating a strong one is preferred (e.g. `pwgen -s 64 1`).
devture_postgres_connection_password: "{{ vault_matrix_postgres_connection_password }}"

matrix_sso: true
# Extra synapse config (mostly for SSO)
# Whitelists are for (respectively):
# - web app on element.interfoodcoop.net
# - mobile app
# - desktop app
# - web app on Nextcloud (nuage.interfoodcoop.net/apps/riotchat/)
matrix_synapse_configuration_extension_yaml: |
  enable_group_creation: true
  auto_join_rooms:
    - '#intercoop:interfoodcoop.net'
    - "#bienvenue:interfoodcoop.net"
    - "#general:interfoodcoop.net"
  sso:
    client_whitelist:
      - "https://element.interfoodcoop.net/"
      - "element://element"
      - "element://vector/webapp/"
      - "element://connect"
      - "https://nuage.interfoodcoop.net"
  password_config:
    enabled: false
  saml2_config:
    sp_config:
      service:
        sp:
          required_attributes:
            - discourse.external_id
            - discourse.username
            - discourse.name
            - discourse.email
            - discourse.groups
      allow_unknown_attributes: true
      attribute_map_dir: "/data/saml2-attribute-maps"
      metadata:
        remote:
          - url: "https://auth.interfoodcoop.net/saml2/idp/metadata.php"
    user_mapping_provider:
      config:
        mxid_source_attribute: "uid"
        mxid_mapping: dotreplace
  experimental_features:
    spaces_enabled: true

# Enable Synapse-admin
# this variable doesn't get read, see the host_vars if you want to change it...
matrix_synapse_admin_enabled: true

# Logging levels
# matrix_synapse_log_level: "INFO"
# matrix_synapse_storage_sql_log_level: "INFO"
# matrix_synapse_root_log_level: "INFO"

# SMTP settings
exim_relay_sender_address: "matrix@interfoodcoop.net"
exim_relay_relay_use: true
exim_relay_relay_host_name: "in-v3.mailjet.com"
exim_relay_relay_host_port: 587
exim_relay_relay_auth: true
exim_relay_relay_auth_username: "af68e16eb6628329a5a8b865c42088d6"
exim_relay_relay_auth_password: "{{ vault_matrix_mailer_relay_auth_password }}"

matrix_client_element_brand: "Messagerie InterFoodcoop"
matrix_client_element_welcome_logo_link: "https://www.interfoodcoop.net"
matrix_client_element_welcome_logo: "https://nuage.interfoodcoop.net/apps/theming/image/logo?useSvg=1&v=29"
matrix_client_element_welcome_headline: "_t('Welcome to Element'). La messagerie instantannée InterFoodcoop "
matrix_client_element_branding_welcome_background_url:  "https://nuage.interfoodcoop.net/apps/theming/image/background?v=29"

matrix_client_element_branding_auth_footer_links:
  - text: "www.interfoodcoop.net"
    url: "https://www.interfoodcoop.net/"
  - text: "Politique de confidentialité"
    url: "https://www.interfoodcoop.net/politique-confidentialite"
  - text: "Mentions légales"
    url: "https://www.interfoodcoop.net/mentions-legales"

matrix_client_element_registration_enabled: false
matrix_client_element_jitsi_preferred_domain: 'framatalk.org'
# Unnecessary for federated homeservers
# matrix_client_element_permalinkPrefix: "https://element.interfoodcoop.net"

# Shared secret Auth password provider for Double pupetting (useful for Bridges)
matrix_synapse_ext_password_provider_shared_secret_auth_enabled: true
matrix_synapse_ext_password_provider_shared_secret_auth_shared_secret: "{{ vault_matrix_synapse_ext_password_provider_shared_secret_auth_shared_secret }}"

# Bridge with Signal
# this variable doesn't get read, see the host_vars if you want to change it...
matrix_mautrix_signal_enabled: true
matrix_mautrix_signal_bridge_encryption_allow: true
matrix_mautrix_signal_configuration_extension_yaml: |
  bridge:
    permissions:
      '@paul_lgds:interfoodcoop.net': admin

# Bridge with WhatsApp
# this variable doesn't get read, see the host_vars if you want to change it...
matrix_mautrix_whatsapp_enabled: true
matrix_mautrix_whatsapp_bridge_encryption_allow: true

# Relay bot
matrix_mautrix_signal_relaybot_enabled: true

# Use traefik as reverse prox
matrix_playbook_reverse_proxy_type: playbook-managed-traefik

devture_traefik_config_certificatesResolvers_acme_email: paul+interfoodcoop@bonaud.fr

# No need to save the vars file on the host it's already public in
# this repository
devture_playbook_state_preserver_enabled: false
