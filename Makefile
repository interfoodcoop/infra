ANSIBLE_MAKEFILE_VERSION:="0.16.0"

.PHONY: install
install:
	pip install -r requirements.txt
	command -v ansible-make >/dev/null || wget -O /tmp/ansible-make-$(ANSIBLE_MAKEFILE_VERSION)_amd64.deb \
	  "https://github.com/paulRbr/ansible-makefile/releases/download/$(ANSIBLE_MAKEFILE_VERSION)/ansible-make_$(ANSIBLE_MAKEFILE_VERSION)_amd64.deb"
	command -v ansible-make >/dev/null || dpkg -i /tmp/ansible-make-$(ANSIBLE_MAKEFILE_VERSION)_amd64.deb
	command -v ansible-make >/dev/null || rm /tmp/ansible-make-$(ANSIBLE_MAKEFILE_VERSION)_amd64.deb
	ansible-make install roles_path=vendor/
	make -C matrix-docker-ansible-deploy roles

.PHONY: dry-run-system-config
dry-run-system-config:
	@echo "→ Go for a dry-run playbook"
	@[ "$(limit)" = "" ] && { \
	  echo "→ Please don't use this target without a limit= variable." \
	  && exit 1; \
	} || { \
	  echo "→ limiting to $(limit)" && exit; \
	}
	HCLOUD_TOKEN=$$(pass terraform/hetzner/intercoop-cloud/token) \
	  ansible-make dry-run env=intercoop-cloud

.PHONY: run-system-config
run-system-config:
	@echo "→ Go for a run playbook"
	@[ "$(limit)" = "" ] && { \
	  echo "→ Please don't use this target without a limit= variable." \
	  && exit 1; \
	} || { \
	  echo "→ limiting to $(limit)" && exit; \
	}
	HCLOUD_TOKEN=$$(pass terraform/hetzner/intercoop-cloud/token) \
	  ansible-make run env=intercoop-cloud

.PHONY: dry-run-nextcloud
dry-run-nextcloud:
	$(MAKE) dry-run-system-config limit=nextcloud

.PHONY: dry-run-collabora
dry-run-collabora:
	$(MAKE) dry-run-system-config limit=collabora

.PHONY: dry-run-matrix
dry-run-matrix:
	$(MAKE) dry-run-system-config limit=matrix tags=setup-all,start

.PHONY: run-nextcloud
run-nextcloud:
	$(MAKE) run-system-config limit=nextcloud

.PHONY: run-collabora
run-collabora:
	$(MAKE) run-system-config limit=collabora

.PHONY: run-matrix
run-matrix:
	$(MAKE) run-system-config limit=matrix tags=setup-all,start

plan-resources:
	tf-make plan provider=hetzner,gandi,scaleway env=intercoop-cloud

apply-resources:
	tf-make apply provider=hetzner,gandi,scaleway env=intercoop-cloud
